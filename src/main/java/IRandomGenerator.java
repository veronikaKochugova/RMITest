import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IRandomGenerator extends Remote {
    int nextRandomNumber() throws RemoteException;
}