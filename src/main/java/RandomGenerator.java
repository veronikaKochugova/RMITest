import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

public class RandomGenerator extends UnicastRemoteObject implements IRandomGenerator {

    private Random random = new Random();

    protected RandomGenerator() throws RemoteException {
    }

    public int nextRandomNumber() throws RemoteException {

        return random.nextInt(100);
    }
}
