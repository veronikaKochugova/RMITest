import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Server {

    public static void main(String[] args) throws NamingException, RemoteException, UnknownHostException, InterruptedException {

        String hostAddress = InetAddress.getLocalHost().getHostAddress();
        int port = 1099;

        if (args.length != 0) {
            port = Integer.valueOf(args[0]);
        }

        LocateRegistry.createRegistry(port);
        Context context = new InitialContext();
        context.bind("rmi://" + hostAddress + ":" + String.valueOf(port) + "//random", new RandomGenerator());
    }
}
