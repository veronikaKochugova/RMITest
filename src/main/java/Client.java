import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.*;

public class Client {
    static private int N;
    static private long waitingTime = TimeUnit.SECONDS.toMillis(100);
    static private String port = "1099";
    static private String ipAddress = "localhost";
    static private String fileName = "../resources/output.txt";

    public static void main(String[] args) throws NamingException, IOException, InterruptedException {

        parseArguments(args);

        Context context = new InitialContext();
        IRandomGenerator randomGenerator =
                (IRandomGenerator)context.lookup("rmi://" + ipAddress + ":" + port + "//random");

        ExecutorService executorService = Executors.newFixedThreadPool(N);

        ArrayList<FutureTask<Long>> threadList = new ArrayList<FutureTask<Long>>();
        for (int i = 0; i < N; ++i) {
            FutureTask<Long> tmp = new FutureTask<Long>(new ExecThread(randomGenerator));
            threadList.add(tmp);
            executorService.submit(tmp);
        }

        Thread.currentThread().sleep(waitingTime);
        executorService.shutdownNow();

        long callsSum = threadList.stream().map((item) -> {
            try {
                return item.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }).mapToLong((item) -> item.longValue()).sum();

        writeToFile(callsSum);

    }

    private static void writeToFile(long callsSum) throws IOException {
        FileWriter fw = new FileWriter(fileName, true);
        //FileWriter fw = new FileWriter("output.txt", true);
        fw.write(N + " " + callsSum / TimeUnit.MILLISECONDS.toSeconds(waitingTime) + "\n");
        fw.close();
    }

    private static void parseArguments(String[] args) {
        if (args.length == 0) {
            System.out.println("No argument: number of threads = 1 by default");
            N = 1;
        } else {
            if (args.length == 3) {
                ipAddress = args[1];
                port = args[2];
            }
            N = Integer.valueOf(args[0]);
        }
    }

    public static class ExecThread implements Callable<Long> {

        private IRandomGenerator generator;
        private Long callsCount = new Long(0);

        public ExecThread(IRandomGenerator generator) {
            this.generator = generator;
        }

        public Long call() throws Exception {
            while (!Thread.interrupted()) {
                try {
                    Integer nextNumber = generator.nextRandomNumber();
                    ++callsCount;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            return callsCount;
        }
    }
}
